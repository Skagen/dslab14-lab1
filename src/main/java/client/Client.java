package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

import cli.Command;
import cli.Shell;
import util.Config;

public class Client implements IClientCli, Runnable {

	private static final String CONNECTION_LOST_MESSAGE 
			= "lost connection to controller";
	
	private static final String IO_ERROR_MESSAGE
			= "I/O Error";
	
	private static final String NOT_LOGGED_IN_MESSAGE
			= "You have to log in first!";
	
	private static final String ALREADY_LOGGED_IN_MESSAGE
			= "You are already logged in!";
	
	private Config config;
	
	private BufferedReader responseReader;
	private PrintWriter    requestWriter;
	
	private String loggedinName = null;
	
	private Socket socket;
	private Shell shell;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Client(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		
		this.config = config;

		this.shell = new Shell("client", userRequestStream, userResponseStream);
		this.shell.register(this);
	}

	@Override
	public void run() {
		
		String controllerHost = config.getString("controller.host");
		String controllerPort = config.getString("controller.tcp.port");
		
		try {
			InetAddress hostAddress = InetAddress.getByName(controllerHost);
			
			socket = new Socket(hostAddress, Integer.parseInt(controllerPort));
			
			responseReader
					= new BufferedReader(
							new InputStreamReader(socket.getInputStream()));
			requestWriter
					= new PrintWriter(socket.getOutputStream());
			
			new Thread(shell).start();
			
		} catch (IOException e) {
			shutdown();
			System.err.println("failed to start client : " + e.getMessage());
		}
	}

	@Command
	@Override
	public String login(String username, String password) {
		
		if (socket.isClosed()) {
			return CONNECTION_LOST_MESSAGE;
		}
		if (loggedinName != null) {
			return ALREADY_LOGGED_IN_MESSAGE;
		}
		
		String loginRequest = "!login " + username + " " + password;
		
		requestWriter.println(loginRequest);
		requestWriter.flush();
		
		String controllerResponse = null; 
		try {
			controllerResponse = responseReader.readLine();
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return "I/O error : " + e.getMessage();
		}
		
		if (controllerResponse == null) {
			closeConnection();
			return CONNECTION_LOST_MESSAGE;
		}
		if (!controllerResponse.startsWith("Error")) {
			loggedinName = username;
		}
		
		return controllerResponse;
	}

	@Command
	@Override
	public String logout() {
		
		if (socket.isClosed()) {
			return CONNECTION_LOST_MESSAGE;
		}
		if (loggedinName == null) {
			return NOT_LOGGED_IN_MESSAGE;
		}
		
		requestWriter.println("!logout ");
		requestWriter.flush();
		
		String controllerResponse = null;
		try {
			controllerResponse = responseReader.readLine(); 
		} catch (IOException e) {
			
		}
		if (controllerResponse == null) {
			closeConnection();
			return CONNECTION_LOST_MESSAGE;
		}
		if (controllerResponse.startsWith("Error")) {
			return "Failed to retrieve credits";
		}
		
		loggedinName = null;
		
		return "Successfully logged out!";
	}

	@Command
	@Override
	public String credits() {
		
		if (socket.isClosed()) {
			return CONNECTION_LOST_MESSAGE;
		}
		if (loggedinName == null) {
			return NOT_LOGGED_IN_MESSAGE;
		}
		
		requestWriter.println("!credits " + loggedinName);
		requestWriter.flush();
		
		String controllerResponse = null;
		try {
			controllerResponse = responseReader.readLine(); 
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return IO_ERROR_MESSAGE;
		}
		
		if (controllerResponse == null) {
			closeConnection();
			return CONNECTION_LOST_MESSAGE;
		}
		
		if (controllerResponse.startsWith("Error")) {
			return controllerResponse;
		}
		
		return "You have " + controllerResponse + " credits!";
	}

	@Command
	@Override
	public String buy(long credits) {
		
		if (socket.isClosed()) {
			return CONNECTION_LOST_MESSAGE;
		}
		if (loggedinName == null) {
			return NOT_LOGGED_IN_MESSAGE;
		}
		
		requestWriter.println("!buy " + credits);
		requestWriter.flush();
		
		String controllerResponse = null;
		try {
			controllerResponse = responseReader.readLine(); 
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return IO_ERROR_MESSAGE;
		}
		
		if (controllerResponse == null) {
			closeConnection();
			return CONNECTION_LOST_MESSAGE;
		}
		
		if (controllerResponse.startsWith("Error")) {
			return controllerResponse;
		}
		
		return "You now have " + controllerResponse + " credits";
	}

	@Command
	@Override
	public String list() {
		
		if (socket.isClosed()) {
			return CONNECTION_LOST_MESSAGE;
		}
		if (loggedinName == null) {
			return NOT_LOGGED_IN_MESSAGE;
		}
		
		requestWriter.println("!list");
		requestWriter.flush();
		
		String controllerResponse = null;
		try {
			controllerResponse = responseReader.readLine(); 
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return IO_ERROR_MESSAGE;
		}
		
		if (controllerResponse == null) {
			closeConnection();
			return CONNECTION_LOST_MESSAGE;
		}
		
		if (controllerResponse.startsWith("Error")) {
			return controllerResponse;
		}
		
		return controllerResponse;
	}

	@Command
	@Override
	public String compute(String term) {
		
		if (socket.isClosed()) {
			return CONNECTION_LOST_MESSAGE;
		}
		if (loggedinName == null) {
			return NOT_LOGGED_IN_MESSAGE;
		}
		
		requestWriter.println("!compute " + term);
		requestWriter.flush();
		
		String controllerResponse = null;
		try {
			controllerResponse = responseReader.readLine(); 
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return IO_ERROR_MESSAGE;
		}
		
		if (controllerResponse == null) {
			closeConnection();
			return CONNECTION_LOST_MESSAGE;
		}
		
		if (controllerResponse.startsWith("Error")) {
			return controllerResponse;
		}
		
		return controllerResponse;
	}

	@Command
	@Override
	public String exit() {
		
		if (loggedinName != null) {
			logout();
		}
		
		shutdown();
		
		return "Shutdown successfull";
	}
	
	private void closeConnection() {
		if (!socket.isClosed()) {
			try {
				socket.close();
			} catch (IOException e) {
				System.err.println("failed to close socket : " + e.getMessage());
			}
		}
	}
	
	public void shutdown() {
		if (socket != null) {
			try {
				socket.close();
			} catch (IOException e1) {
				System.err.println("failed to close socket : " + e1.getMessage());
			}
		}
		if (responseReader != null) {
			try {
				responseReader.close();
			} catch (IOException e1) {
				System.err.println("failed to close response input stream : " + e1.getMessage());
			}
		}
		if (requestWriter != null) {
			requestWriter.close();
		}
		if (shell != null) {
			shell.close();
		}
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Client} component
	 */
	public static void main(String[] args) {
		Client client = new Client(args[0], new Config("client"), System.in,
				System.out);
		new Thread(client).start();
	}

	// --- Commands needed for Lab 2. Please note that you do not have to
	// implement them for the first submission. ---

	@Override
	public String authenticate(String username) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
