package controller;

import java.net.DatagramSocket;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import controller.node.Node;
import controller.node.NodeListener;
import controller.node.NodeUpdater;
import controller.node.Operation;

public class NodeManager implements Runnable {
	
	private DatagramSocket socket;
	private List<Node> nodes = Collections.synchronizedList(new LinkedList<Node>());
	
	private Thread nodeUpdaterThread = null;
	private ControllerConfig config;
	
	public NodeManager(CloudController controller, DatagramSocket socket) {
		this.socket = socket;
		this.config = controller.getControllerConfig();
	}

	@Override
	public void run() {
		new Thread(new NodeListener(socket, nodes)).start();
		nodeUpdaterThread = new Thread(new NodeUpdater(nodes, config));
		nodeUpdaterThread.start();
	}
	
	/**
	 * @return a synchronized list of all known nodes.
	 */
	public List<Node> getNodes() {
		return nodes;
	}
	
	public void close() {
		if (socket != null) {
			socket.close();
		}
		nodeUpdaterThread.interrupt();
	}
	
	public List<Node> getNodesForOperator(Operation operator) {
		List<Node> nodes = new LinkedList<Node>();
		synchronized (this.nodes){ 
			for (Node node : this.nodes) {
				if (node.supports(operator)) {
					nodes.add(node);
				}
			}
		}
		Collections.sort(nodes, new UsageAscending());
		
		return nodes;
	}
	
	private class UsageAscending implements Comparator<Node> {

		@Override
		public int compare(Node o1, Node o2) {
			if (o1.getUsage() > o2.getUsage()) {
				return 1;
			} else if (o1.getUsage() < o2.getUsage()) {
				return -1;
			}
			
			return 0;
		}
		
	}
	
	public List<Operation> getAvailableOperators() {
		
		List<Operation> supportedOperations = new LinkedList<Operation>();
		
		synchronized (nodes) {
			for (Node node : nodes) {
				for (Operation operation : node.getOperations()) {
					if (!supportedOperations.contains(operation)) {
						supportedOperations.add(operation);
					}
				}
			}
		}
		
		return supportedOperations;
	}
}
