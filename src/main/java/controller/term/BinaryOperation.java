package controller.term;

public abstract class BinaryOperation implements Term {

	private Term left;
	private Term right;
	
	public BinaryOperation(Term left, Term right) {
		this.left  = left;
		this.right = right;
	}
	
	public Term left() {
		return left;
	}
	
	public Term right() {
		return right;
	}
}
