package controller.term;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import controller.NodeManager;
import controller.node.Node;
import controller.node.Operation;
import static controller.node.Operation.*;

public class TermEvaluator implements TermVisitor<String> {

	private final int RETRY_ATTEMPTS = 3;
	
	private final int RESPONSE_TIMEOUT = 800; // ms
	
	private String result = null;
	
	private NodeManager nodeManager;
	
	public TermEvaluator(NodeManager nodeManager) {
		this.nodeManager = nodeManager;
	}
	
	public String getResult() {
		return result;
	}
	
	@Override
	public TermEvaluator dispatchVisit(Term term) {
		result = term.accept(this);
		return this;
	}

	@Override
	public String visit(Addition sum) {
		String left = sum.left().accept(this);
		if (left.startsWith("Error")) {
			return left;
		}
		String right = sum.right().accept(this);
		if (right.startsWith("Error")) {
			return right;
		}
		return scheduleEvaluation(left + " + " + right, Addition);
	}

	@Override
	public String visit(Subtraction term) {
		String left = term.left().accept(this);
		if (left.startsWith("Error")) {
			return left;
		}
		String right = term.right().accept(this);
		if (right.startsWith("Error")) {
			return right;
		}
		return scheduleEvaluation(left + " - " + right, Subtraction);
	}

	@Override
	public String visit(Multiplication term) {
		String left = term.left().accept(this);
		if (left.startsWith("Error")) {
			return left;
		}
		String right = term.right().accept(this);
		if (right.startsWith("Error")) {
			return right;
		}
		return scheduleEvaluation(left + " * " + right, Multiplication);
	}

	@Override
	public String visit(Division term) {
		String left = term.left().accept(this);
		if (left.startsWith("Error")) {
			return left;
		}
		String right = term.right().accept(this);
		if (right.startsWith("Error")) {
			return right;
		}
		return scheduleEvaluation(left + " / " + right, Division);
	}

	@Override
	public String visit(Negation negate) {
		return "-" + negate.operand().accept(this);
	}

	@Override
	public String visit(Number number) {
		return number.literal();
	}
	
	/**
	 * Schedules the evaluation of a term by a node.
	 * @param term the term to evaluate by a node
	 * @param operator the operator required to perform the evaluation
	 * @return the response from the node, or an error message if the some errors
	 * occurred.
	 */
	private String scheduleEvaluation(String term, Operation operator) {
		
		String response = null;
		
		int remainingAttempts = RETRY_ATTEMPTS;
		List<Node> nodes = nodeManager.getNodesForOperator(operator);
		Connection connection = null;
		
		while (remainingAttempts > 0 && response == null) {
			remainingAttempts--;
			
			connection = establishConnection(nodes);
			if (connection == null) {
				return "Error: operation cannot be performed";
			}
			response = requestEvaluation(term, connection);
			
			connection.close();
		}
		
		if (response == null) {
			response = "Error: nodes unreacheable";
		}
		
		if (!response.startsWith("Error")) {
			connection.getNode().updateUsage(response);
		}
		
		return response;
	}
	
	/**
	 * Establishes a connection to the available node with the least usage.
	 * Removes unavailable nodes from node list.
	 * 
	 * @param nodes the nodes to try to connect to
	 * @return a connection or null if no node is available
	 */
	private Connection establishConnection(List<Node> nodes) {
		
		Connection connection = null;
		
		Iterator<Node> it = nodes.iterator();
		
		while (it.hasNext() && connection == null) {
			Node node = it.next();
			it.remove();
			try {
				Socket socket = new Socket(node.getInetAddr(), node.getTcpPort());
				connection = new Connection(node, socket);
			} catch (IOException e) {
				// ignore exception
			}
		}
		
		return connection;
	}
	
	/**
	 * Sends a compute command for a term using a connection.
	 * 
	 * @param term the term to use for the compute command
	 * @param connection the connection to send the command to
	 * @return a response or null if some error occurred
	 */
	private String requestEvaluation(String term, Connection connection) {
		
		String response = null;
		
		try {
			PrintWriter writer 
					= new PrintWriter(connection.getSocket().getOutputStream());
			
			BufferedReader in 
				= new BufferedReader(
					new InputStreamReader(connection.getSocket().getInputStream()));
			
			writer.println("!compute " + term);
			writer.flush();
			
			FutureTask<String> responseRecieveTask 
					= new FutureTask<String> (new ResponseReciever(in));
			
			new Thread(responseRecieveTask).start();
			
			response = responseRecieveTask.get(RESPONSE_TIMEOUT, TimeUnit.MILLISECONDS);
			
		} catch (IOException | InterruptedException | ExecutionException | TimeoutException e) {
			// ignore exception
		}
		
		return response;
	}
	
	private class ResponseReciever implements Callable<String> {

		private BufferedReader in;
		
		public ResponseReciever(BufferedReader in) {
			this.in = in;
		}
		
		@Override
		public String call() throws Exception {
			return in.readLine();
		}
	}
	
	private class Connection {
		
		private Socket socket;
		private Node node;
		
		public Connection(Node node, Socket socket) {
			this.socket = socket;
			this.node 	= node;
		}
		
		public Socket getSocket() {
			return socket;
		}
		
		public Node getNode() {
			return node;
		}
		
		public void close() {
			try {
				if (socket != null && !socket.isClosed()) {
					socket.close();
				}
			} catch (IOException e) {
				System.err.println("failed to close socket properly");
			}
		}
	}
}
