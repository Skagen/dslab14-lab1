package controller.term;

public class TermCostEvaluator implements TermVisitor<Integer> {

	private int result = 0; 
	
	public int getResult() {
		return result;
	}
	
	@Override
	public TermCostEvaluator dispatchVisit(Term term) {
		result = 50 * term.accept(this);
		return this;
	}

	@Override
	public Integer visit(Addition sum) {
		return sum.left().accept(this) + sum.right().accept(this) + 1;
	}

	@Override
	public Integer visit(Subtraction difference) {
		return difference.left().accept(this) + difference.right().accept(this) + 1;
	}

	@Override
	public Integer visit(Multiplication product) {
		return product.left().accept(this) + product.right().accept(this) + 1;
	}

	@Override
	public Integer visit(Division quotient) {
		return quotient.left().accept(this) + quotient.right().accept(this) + 1;
	}

	@Override
	public Integer visit(Negation negate) {
		return 0;
	}

	@Override
	public Integer visit(Number number) {
		return 0;
	}
}
