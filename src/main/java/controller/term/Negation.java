package controller.term;

public class Negation implements Term {

	private Term operand;
	
	public Negation(Term operand) {
		this.operand = operand;
	}
	
	@Override
	public <R> R accept(TermVisitor<R> visitor) {
		return visitor.visit(this);
	}

	public Term operand() {
		return operand;
	}
}
