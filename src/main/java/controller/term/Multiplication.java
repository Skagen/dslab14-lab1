package controller.term;

public class Multiplication extends BinaryOperation {
	
	public Multiplication(Term left, Term right) {
		super(left, right);
	}
	
	@Override
	public <R> R accept(TermVisitor<R> visitor) {
		return visitor.visit(this);
	}
	
}
