package controller.term;

public class Division extends BinaryOperation {
	
	public Division(Term left, Term right) {
		super(left, right);
	}
	
	@Override
	public <R> R accept(TermVisitor<R> visitor) {
		return visitor.visit(this);
	}
	
}
