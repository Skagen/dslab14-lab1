package controller.term;

public class Addition extends BinaryOperation {
	
	public Addition(Term left, Term right) {
		super(left, right);
	}
	
	@Override
	public <R> R accept(TermVisitor<R> visitor) {
		return visitor.visit(this);
	}
	
}
