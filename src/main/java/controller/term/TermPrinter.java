package controller.term;

public class TermPrinter implements TermVisitor<String> {

	private String result = null;

	public String getResult() {
		return result;
	}
	
	@Override
	public TermPrinter dispatchVisit(Term term) {
		result = term.accept(this);
		return this;
	}

	@Override
	public String visit(Addition sum) {
		String left  = sum.left().accept(this);
		String right = sum.right().accept(this);
		return "(" + left + " + " + right + ")";
	}

	@Override
	public String visit(Subtraction difference) {
		String left  = difference.left().accept(this);
		String right = difference.right().accept(this);
		return "(" + left + " - " + right + ")";
	}

	@Override
	public String visit(Multiplication product) {
		String left  = product.left().accept(this);
		String right = product.right().accept(this);
		return "(" + left + " * " + right + ")";
	}

	@Override
	public String visit(Division quotient) {
		String left  = quotient.left().accept(this);
		String right = quotient.right().accept(this);
		return "(" + left + " / " + right + ")";
	}

	@Override
	public String visit(Negation negate) {
		return "-" + negate.operand().accept(this);
	}

	@Override
	public String visit(Number number) {
		return number.literal();
	}
	
}
