package controller.term;

public interface Term {

	public <R> R accept(TermVisitor<R> visitor);
}
