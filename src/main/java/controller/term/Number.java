package controller.term;

public class Number implements Term {

	private String literal;
	
	public Number(String literal) {
		this.literal = literal;
	}
	
	@Override
	public <R> R accept(TermVisitor<R> visitor) {
		return visitor.visit(this);
	}

	public String literal() {
		return literal;
	}
}
