package controller.term;

public class Subtraction extends BinaryOperation {
	
	public Subtraction(Term left, Term right) {
		super(left, right);
	}
	
	@Override
	public <R> R accept(TermVisitor<R> visitor) {
		return visitor.visit(this);
	}
	
}
