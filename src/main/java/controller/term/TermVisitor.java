package controller.term;

public interface TermVisitor<R> {

	public TermVisitor<R> dispatchVisit(Term term);
	
	public R visit(Addition sum);
	
	public R visit(Subtraction difference);
	
	public R visit(Multiplication product);
	
	public R visit(Division quotient);
	
	public R visit(Negation negate);

	public R visit(Number number);
}
