package controller;

import util.Config;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;

import controller.node.Node;
import cli.Command;
import cli.Shell;

public class CloudController implements ICloudControllerCli, Runnable {
	
	private ControllerConfig controllerConfig;
	private UserConfig userConfig;

	private Shell shell;
	
	private DatagramSocket nodeServer;
	private ServerSocket clientServer;
	
	private ClientManager clientManager;
	private NodeManager nodeManager;
	
	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public CloudController(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		
		this.controllerConfig = new ControllerConfig(config);
		this.userConfig = new UserConfig(new Config("user"));
		
		this.shell = new Shell(componentName, userRequestStream, userResponseStream);
		this.shell.register(this);
	}
	
	public NodeManager getNodeManager() {
		return nodeManager;
	}
	
	public ClientManager getClientManager() {
		return clientManager;
	}
	
	public UserConfig getUserConfig() {
		return userConfig;
	}
	
	public ControllerConfig getControllerConfig() {
		return controllerConfig;
	}

	@Override
	public void run() {
		try {
			nodeServer 	  = new DatagramSocket(controllerConfig.getUdpPort());
			clientServer  = new ServerSocket(controllerConfig.getTcpPort());
			
			clientManager = new ClientManager(this, clientServer);
			nodeManager   = new NodeManager(this, nodeServer);
			
			new Thread(clientManager).start();
			new Thread(nodeManager).start();
			new Thread(shell).start();
			
		} catch (IOException e) {
			shutdown();
		}
	}

	@Command
	@Override
	public String nodes() throws IOException {
		String response = "";
		if (nodeManager.getNodes().size() == 0) {
			return "No nodes";
		}
		synchronized (nodeManager.getNodes()) {
			for (Node node : nodeManager.getNodes()) {
				response += node.getStatus() + "\n";
			}
		}
		
		return response.trim();
	}

	@Command
	@Override
	public String users() throws IOException {
		String response = "";
		if (clientManager.getClients().size() == 0) {
			return "No users";
		}
		synchronized (clientManager.getClients()) {
			for (Client client : clientManager.getClients()) {
				response += client.getStatus() + "\n";
			}
		}
		return response.trim();
	}

	@Command
	@Override
	public String exit() throws IOException {
		shutdown();
		return null;
	}
	
	public void shutdown() {
		try {
			if (clientServer != null && !clientServer.isClosed()) {
				clientServer.close();
			}
		} catch (IOException e) {
			System.err.println("failed to close socket : " + e.getMessage());
		}
		if (nodeServer != null && !nodeServer.isClosed()) {
			nodeServer.close();
		}
		if (clientManager != null) {
			clientManager.close();
		}
		if (nodeManager != null) {
			nodeManager.close();	
		}
		if (shell != null) {
			shell.close();
		}
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link CloudController}
	 *            component
	 */
	public static void main(String[] args) {
		CloudController cloudController = new CloudController(args[0],
				new Config("controller"), System.in, System.out);
		new Thread(cloudController).start();
	}

}

