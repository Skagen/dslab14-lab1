package controller.parser;

import java.util.LinkedList;
import java.util.List;

import static controller.parser.TokenType.*;


public class TermLexer {

	public List<Token> lex(String term) throws TermSyntaxException {
		
		List<String> input = new Tokenizer("\\+", "-", "\\*", "/").tokenize(term);
		List<Token> tokens = new LinkedList<Token>();
		
		for (String token : input) {
			if (token.matches("^\\+$")) {
				tokens.add(new Token(Plus, token));
			} else if (token.matches("^/$")) {
				tokens.add(new Token(Divide, token));
			} else if (token.matches("^\\*$")) {
				tokens.add(new Token(Times, token));
			} else if (token.matches("^-$")) {
				tokens.add(new Token(Minus, token));
			} else if (token.matches("[0-9]+")) {
				tokens.add(new Token(Number, token));
			} else {
				throw new TermSyntaxException("Invalid token : " + token);
			}
		}
		
		return tokens;
	}
}
