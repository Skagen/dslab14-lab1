package controller.parser;

public enum TokenType {
	Number,
	Plus,
	Minus,
	Times,
	Divide
}
