package controller.parser;

public class TermSyntaxException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public TermSyntaxException(String message) {
		super(message);
	}
}
