package controller.parser;

public class Token {

	private TokenType type;
	private String lexeme;
	
	public Token(TokenType type, String lexeme) {
		this.type = type;
		this.lexeme = lexeme;
	}
	
	public TokenType type() {
		return type;
	}
	
	public String lexeme() {
		return lexeme;
	}
	
	@Override
	public String toString() {
		return "(" + type + "," + lexeme + ")";
	}
}
