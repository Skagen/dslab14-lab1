package controller.parser;

import java.util.Iterator;

import controller.term.*;
import controller.term.Number;
import static controller.parser.TokenType.*;

public class TermParser {

	private Token currentToken = null;
	
	private Iterator<Token> tokenIterator = null; 
	
	public Term parse(String term) throws TermSyntaxException {
		tokenIterator = new TermLexer().lex(term).iterator();
		currentToken = tokenIterator.hasNext() ? tokenIterator.next() : null;
		Term result = term_1();
		if (currentToken != null) {
			throw new TermSyntaxException("Unexpected token " + currentToken.lexeme());
		}
		
		return result;
	}
	
	private Term term_1() throws TermSyntaxException {
		
		Term term = term_2();
		
		while (currentToken != null) {
			if (accept(Plus)) {
				Term right = number();
				term = new Addition(term, right);
			} else if (accept(Minus)) {
				Term right = number();
				term = new Subtraction(term, right);
			} else if (accept(Times)) {
				Term right = number();
				term = new Multiplication(term, right);
			} else if (accept(Divide)) {
				Term right = number();
				term = new Division(term, right);
			} else {
				throw new TermSyntaxException("Expected '+,-,*,/,' but got " + printToken(currentToken));
			}
		}
		
		return term;
	}
	
	private Term term_2() throws TermSyntaxException {

		Term left = number();
		
		if (currentToken == null) {
			return left;
		}
		
		Term term = null;
		if (accept(Plus)) {
			Term right = number();
			term = new Addition(left, right);
		} else if (accept(Minus)) {
			Term right = number();
			term = new Subtraction(left, right);
		} else if (accept(Times)) {
			Term right = number();
			term = new Multiplication(left, right);
		} else if (accept(Divide)) {
			Term right = number();
			term = new Division(left, right);
		} else {
			throw new TermSyntaxException("Expected operator but got " + printToken(currentToken));
		}
		
		return term;
	}
	
	private Term number() throws TermSyntaxException {
		if (accept(Minus)) {
			Term operand = number();
			return new Negation(operand);
		}
		Token number = currentToken;
		if (!accept(Number)) {
			throw new TermSyntaxException("Expected '-' or number but got " + printToken(currentToken));
		}
		return new Number(number.lexeme());
	}
	
	private boolean accept(TokenType type) {
		if (currentToken == null) {
			return false;
		}
		if (currentToken.type().equals(type)) {
			currentToken = (tokenIterator.hasNext()) ? tokenIterator.next() : null;
			return true;
		}
		return false;
	}
	
	private String printToken(Token token) {
		return token == null ? "nothing" : token.lexeme();
	}
	
	public static void main(String[] args) {
		String[] terms = {
				"1",
				"-1",
				"3 * - 1",
				"3 + - 2 / 4",
				"3 + - 2 / 4 + 3 + - 2 / 4 + 3 + - 2 / 4",
				"- - - 1",
				"1 - - 1",
				"1 *",
				"* 3",
				"",
		};
		for (String term : terms) {
			try {
				System.out.println(new TermPrinter().dispatchVisit(new TermParser().parse(term)).getResult());
			} catch (TermSyntaxException e) {
				System.out.println(e.getMessage());
			}
		}
	}
}
