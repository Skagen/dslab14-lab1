package controller;

public class Client {
	
	private String username;
	private int credits;
	private boolean loggedin;
	
	public Client(String username, int credits, boolean loggedin) {
		this.username = username;
		this.credits  = credits;
		this.loggedin = loggedin;
	}
	
	public synchronized String getUsername() {
		return username;
	}
	
	public synchronized int getCredits() {
		return credits;
	}
	
	public synchronized boolean isLoggedin() {
		return loggedin;
	}
	
	public synchronized String getStatus() {
		return username + " " + (loggedin ? "online" : "offline") + " Credits: " + credits;
	}
	
	public synchronized void increaseCredits(int credits) {
		this.credits += credits;
	}
	
	public synchronized boolean withdrawCredits(int withdraw) {
		if (credits < withdraw) {
			return false;
		}
		credits -= withdraw;
		
		return true;
	}
	
	public synchronized void setLoggedin(boolean loggedin) {
		this.loggedin = loggedin;
	}
	
	@Override
	public synchronized boolean equals(Object other) {
		return other != null
				&& other instanceof Client
				&& ((Client) other).username.equals(username);
	}
}
