package controller;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ClientManager implements Runnable {
	
	private List<Client> clients = Collections.synchronizedList(new LinkedList<Client>());
	private List<Socket> connections = new LinkedList<Socket>();
	private ExecutorService threadPool = Executors.newFixedThreadPool(10);
	
	private UserConfig userConfig;
	
	private ServerSocket socket;
	private CloudController controller;
	
	public ClientManager(CloudController controller, ServerSocket socket) { 
		this.controller = controller;
		this.socket		= socket;
		this.userConfig = controller.getUserConfig();
		loadClients();
	}
	
	/**
	 * @return a synchronized list of all known clients.
	 */
	public List<Client> getClients() {
		return clients;
	}
	
	public Client loginClient(String username, String password) {
		if (!userConfig.getUsernames().contains(username)) {
			return null;
		}
		if (!password.equals(userConfig.getPassword(username))) {
			return null;
		}
		int credits = userConfig.getCredits(username);
		synchronized (clients) {
			for (Client client : clients) {
				if (client.getUsername().equals(username)) {
					client.setLoggedin(true);
					return client;
				}
			}
		}
		Client client = new Client(username, credits, true);
		clients.add(client);
		return client;
	}

	@Override
	public void run() {
		try {
			while (!Thread.currentThread().isInterrupted()) {
				Socket connection = socket.accept();
				connections.add(connection);
				try {
					threadPool.execute(new CommandProcessor(controller, connection));
				} catch (IOException e) {
					System.out.println("client socket failed : " + e.getMessage());
				}
				updateConnections();
			}
		} catch (IOException e) {
			// ignored because it is handled by the finally block
		} finally {
			close();
		}
	}
	
	private synchronized void updateConnections() {
		Iterator<Socket> it = connections.iterator();
		while(it.hasNext()) {
			Socket connection = it.next();
			if (connection != null && connection.isClosed()) {
				it.remove();
			}
		}
	}
	
	private synchronized void closeAllConnections() {
		Iterator<Socket> it = connections.iterator();
		while(it.hasNext()) {
			Socket connection = it.next();
			if (connection != null) {
				try {
					connection.close();
				} catch (IOException e) {
					System.err.println("failed to close client connection");
				}
				it.remove();
			}
		}
	}
	
	private void loadClients() {
		for (String username : userConfig.getUsernames()) {
			clients.add(new Client(
					username,
					userConfig.getCredits(username),
					false
			));
		}
	}
	
	public void close() {
		threadPool.shutdown();
		try {
			socket.close();
		} catch (IOException e) {
			System.err.println("Failed to close client server socket");
		}
		closeAllConnections();
	}
}
