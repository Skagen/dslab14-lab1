package controller.commands;

import controller.Client;
import controller.CommandProcessor;

public class BuyCommand implements Command {

	private int credits;
	
	public BuyCommand(int credits) {
		this.credits = credits;
	}
	
	@Override
	public String execute(CommandProcessor processor) {
		Client client = processor.getClient();
		if (client == null || !client.isLoggedin()) {
			return "Error: You are not logged in";
		}
		client.increaseCredits(credits);
		
		return "" + client.getCredits();
	}
}
