package controller.commands;

public class LogoutCommandFactory implements CommandFactory {

	@Override
	public Command create(String[] args) throws CommandFactoryException {
		if (args.length != 1) {
			throw new CommandFactoryException("Invalid arguments");
		}
		return new LogoutCommand();
	}

}
