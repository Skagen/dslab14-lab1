package controller.commands;

import controller.CommandProcessor;

public interface Command {

	public String execute(CommandProcessor processor);
}
