package controller.commands;

public interface CommandFactory {

	public Command create(String[] args) throws CommandFactoryException;
}
