package controller.commands;

import controller.Client;
import controller.CommandProcessor;
import controller.NodeManager;
import controller.term.Term;
import controller.term.TermCostEvaluator;
import controller.term.TermEvaluator;

public class ComputeCommand implements Command {

	private Term term;
	
	public ComputeCommand(Term term) {
		this.term = term;
	}
	
	@Override
	public String execute(CommandProcessor processor) {
		Client client = processor.getClient();
		if (client == null || !client.isLoggedin()) {
			return "Error: you are not logged in";
		}
		int cost = new TermCostEvaluator().dispatchVisit(term).getResult();
		if (!client.withdrawCredits(cost)) {
			return "Error: not enough credits";
		}
		NodeManager nodeManager = processor.getController().getNodeManager();
		String result = new TermEvaluator(nodeManager).dispatchVisit(term).getResult();
		if (result.startsWith("Error")) {
			client.increaseCredits(cost);
		}
		return result;
	}
}
