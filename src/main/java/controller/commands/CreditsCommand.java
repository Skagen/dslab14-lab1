package controller.commands;

import controller.Client;
import controller.CommandProcessor;

public class CreditsCommand implements Command {
	
	@Override
	public String execute(CommandProcessor processor) {
		Client client = processor.getClient();
		if (client == null || !client.isLoggedin()) {
			return "Error: Not logged in";
		}
		
		return "" + processor.getClient().getCredits();
	}
}
