package controller.commands;

public class BuyCommandFactory implements CommandFactory {

	@Override
	public Command create(String[] args) throws CommandFactoryException {
		if (args.length != 2) {
			throw new CommandFactoryException("Invalid arguments");
		}
		if (!args[1].matches("^[0-9]+$")) {
			throw new CommandFactoryException("Invalid argument : not an integer number");
		}
		return new BuyCommand(Integer.parseInt(args[1]));
	}

}
