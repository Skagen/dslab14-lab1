package controller.commands;

import controller.parser.TermParser;
import controller.parser.TermSyntaxException;
import controller.term.Term;

public class ComputeCommandFactory implements CommandFactory {

	@Override
	public Command create(String[] args) throws CommandFactoryException {
		if (args.length < 1) {
			throw new CommandFactoryException("Error: invalid arguements");
		}
		Term term = null;
		try {
			term = new TermParser().parse(assembleTerm(args));
		} catch (TermSyntaxException e) {
			throw new CommandFactoryException("Error: " + e.getMessage());
		}
		return new ComputeCommand(term);
	}

	private String assembleTerm(String[] args) {
		String term = "";
		for (int i = 1; i < args.length; i++) {
			term += args[i] + " ";
		}
		return term.trim();
	}
}
