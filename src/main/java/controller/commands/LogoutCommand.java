package controller.commands;

import controller.Client;
import controller.CommandProcessor;

public class LogoutCommand implements Command {

	@Override
	public String execute(CommandProcessor processor) {
		Client client = processor.getClient();
		if (client == null || !client.isLoggedin()) {
			return "Error: You have to be logged in";
		}
		client.setLoggedin(false);
		
		return "Successfully logged out!";
	}
}
