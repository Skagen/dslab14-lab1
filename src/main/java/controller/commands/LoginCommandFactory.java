package controller.commands;

public class LoginCommandFactory implements CommandFactory {

	@Override
	public Command create(String[] args) throws CommandFactoryException {
		if (args.length != 3) {
			throw new CommandFactoryException("Invalid arguments");
		}
		return new LoginCommand(args[1], args[2]);
	}

}
