package controller.commands;

import controller.Client;
import controller.CommandProcessor;

public class LoginCommand implements Command {

	private String username;
	private String password;
	
	public LoginCommand(String username, String password) {
		this.username = username;
		this.password = password;
	}
	
	@Override
	public String execute(CommandProcessor processor) {
		Client client = processor.getClient();
		if (client != null && client.isLoggedin()) {
			return "Error : You are already logged in!";
		}
		client = processor.getController().getClientManager().loginClient(username, password);
		if (client == null) {
			return "Error : Wrong username or password.";
		}
		client.setLoggedin(true);
		processor.setClient(client);
		
		return "Successfully logged in.";
	}
}
