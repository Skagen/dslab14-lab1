package controller.commands;

import controller.Client;
import controller.CommandProcessor;
import controller.node.Operation;

public class ListCommand implements Command {

	@Override
	public String execute(CommandProcessor processor) {
		Client client = processor.getClient();
		if (client == null || !client.isLoggedin()) {
			return "Error: Not logged in";
		}
		String response = "";
		for (Operation operation : processor.getController().getNodeManager().getAvailableOperators()) {
			response += operation.toString();
		}
		
		return response; 
	}

}
