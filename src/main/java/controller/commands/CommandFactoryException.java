package controller.commands;

public class CommandFactoryException extends Exception {

	private static final long serialVersionUID = 1L;

	public CommandFactoryException(String message) {
		super(message);
	}
}
