package controller.commands;

public class CreditsCommandFactory implements CommandFactory {

	@Override
	public Command create(String[] args) throws CommandFactoryException {
		return new CreditsCommand();
	}

}
