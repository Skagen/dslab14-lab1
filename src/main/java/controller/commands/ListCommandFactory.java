package controller.commands;

public class ListCommandFactory implements CommandFactory {

	@Override
	public Command create(String[] args) throws CommandFactoryException {
		if (args.length != 1) {
			throw new CommandFactoryException("invalid arguments");
		}
		return new ListCommand();
	}

}
