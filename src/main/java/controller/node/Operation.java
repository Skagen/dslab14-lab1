package controller.node;

public enum Operation {
	Division,
	Addition,
	Subtraction,
	Multiplication;
	
	@Override
	public String toString() {
		
		String result = "";
		
		switch(this) {
		case Division:
			result = "/";
			break;
		
		case Subtraction:
			result = "-";
			break;
		
		case Addition:
			result = "+";
			break;
			
		case Multiplication:
			result = "*";
			break;
		}
		
		return result;
	}
}
