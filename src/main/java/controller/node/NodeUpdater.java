package controller.node;

import java.util.List;

import controller.ControllerConfig;

public class NodeUpdater implements Runnable {

	private List<Node> nodes;
	
	private int aliveTimeout;
	
	private int checkPeriod;
	
	public NodeUpdater(List<Node> nodes, ControllerConfig config) {
		this.nodes = nodes;
		this.aliveTimeout = config.getNodeTimeout();
		this.checkPeriod  = config.getCheckPeriod();
	}
	
	@Override
	public void run() {
		try {
			while (!Thread.currentThread().isInterrupted()) {
				
				Thread.sleep(checkPeriod);
				
				long currentTime = System.currentTimeMillis();
				for (Node node : nodes) {
					if (node.isOnline() && currentTime - node.getLastAlive() > aliveTimeout) {
						node.setOnline(false);
					}
				}
			}
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

}
