package controller.node;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static controller.node.Operation.*;

public class NodeListener implements Runnable {

	private static int BUFFER_SIZE = 128;
	
	private static String USAGE = "Error: !alive <tcp-port> [<operations>]";
	
	private List<Node> nodes;
	private DatagramSocket socket;
	
	public NodeListener(DatagramSocket socket, List<Node> nodes) {
		this.nodes = nodes;
		this.socket = socket;
	}
	
	@Override
	public void run() {
		byte[] buffer = new byte[BUFFER_SIZE];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
		try {
			while (!Thread.currentThread().isInterrupted()) {
				socket.receive(packet);
				handleRequest(packet, buffer);
			}
		} catch (IOException e) {
			// ignored because it cannot be handled
		} finally {
			if (socket != null && !socket.isClosed()) {
				socket.close();
			}
		}
	}

	private void handleRequest(DatagramPacket packet, byte[] buffer) throws IOException {
		String request = new String(Arrays.copyOf(packet.getData(), packet.getLength()));
		String[] args  = request.split("\\s+");
		
		if (args.length < 2 || args.length > 3) {
			sendUsage(packet, buffer);
			return;
		}
		if (!args[0].equals("!alive")) {
			sendUsage(packet, buffer);
			return;
		}
		if (!args[1].matches("[0-9]+")) {
			sendUsage(packet, buffer);
			return;
		}
		if (args.length == 3 && !args[2].matches("[\\/\\+\\-\\*]*")) {
			sendUsage(packet, buffer);
			return;
		}
		
		updateNode(packet, args);
	}
	
	private void sendUsage(DatagramPacket packet, byte[] buffer) throws IOException {
		byte[] usage = USAGE.getBytes();
		InetAddress adress = packet.getAddress();
		int port = packet.getPort();
		DatagramPacket newPacket = new DatagramPacket(usage, usage.length, adress, port);
		socket.send(newPacket);
	}
	
	private void updateNode(DatagramPacket packet, String[] args) {
		InetAddress addr = packet.getAddress();
		int port         = Integer.parseInt(args[1]);
		List<Operation> operations = new LinkedList<Operation>();
		if (args.length == 3) {
			for (int i = 0; i < args[2].length(); i++) {
				Operation operation = null;
				switch (args[2].charAt(i)) {
				case '+':
					operation = Addition;
					break;
				case '-':
					operation = Subtraction;
					break;
				case '*':
					operation = Multiplication;
					break;
				case '/':
					operation = Division;
					break;
				}
				if (!operations.contains(operation)) {
					operations.add(operation);
				}
			}
		}
		Node node = null;
		for (Node n : nodes) {
			if (n.getInetAddr().equals(addr) && n.getTcpPort() == port) {
				node = n;
				break;
			}
		}
		if (node != null) {
			node.update(addr, port, operations, true, System.currentTimeMillis());
		} else {
			nodes.add(new Node(addr, port, operations, true, System.currentTimeMillis()));
		}
	}
}
