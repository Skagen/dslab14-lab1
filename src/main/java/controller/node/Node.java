package controller.node;

import java.net.InetAddress;
import java.util.List;

public class Node {

	private int tcpPort;
	private InetAddress inetAddr;
	private List<Operation> operations;
	private boolean online;
	private int usage = 0;
	private long lastAlive;
	
	public Node(InetAddress addr, int port, List<Operation> operations, boolean online, long lastAlive) {
		this.inetAddr 	= addr;
		this.tcpPort  	= port;
		this.operations = operations;
		this.online 	= online;
		this.lastAlive 	= lastAlive;
	}
	
	public int getTcpPort() {
		return tcpPort;
	}
	
	public InetAddress getInetAddr() {
		return inetAddr;
	}
	
	public boolean isOnline() {
		return online;
	}
	
	public synchronized int getUsage() {
		return usage;
	}
	
	public synchronized void updateUsage(String response) {
		if (response.startsWith("Error")) {
			return;
		}
		if (response.length() > 0 && response.charAt(0) == '-') {
			usage += 50 * (response.length() - 1);
		} else  {
			usage += 50 * response.length();
		}
	}
	
	public synchronized boolean supports(Operation operation) {
		return operations.contains(operation);
	}
	
	public void setOperations(List<Operation> operations) {
		this.operations = operations;
	}
	
	public synchronized void setOnline(boolean online) {
		this.online = online;
	}
	
	public synchronized long getLastAlive() {
		return lastAlive;
	}
	
	public synchronized List<Operation> getOperations() {
		return operations;
	}
	
	public synchronized void 
	update(InetAddress addr, int port, List<Operation> operations, boolean online, long lastAlive) {
		this.inetAddr = addr;
		this.tcpPort  = port;
		this.operations = operations;
		this.online = online;
		this.lastAlive = lastAlive;
	}
	
	public synchronized String getStatus() {
		return "IP: " + inetAddr.getHostAddress() 
				+ " Port: " + tcpPort 
				+ " " + (online ? "online" : "offline")
				+ " usage: " + usage;
	}
	
	@Override
	public String toString() {
		return "" + usage;
	}
}
