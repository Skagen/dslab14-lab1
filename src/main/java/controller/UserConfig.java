package controller;

import java.util.LinkedList;
import java.util.List;

import util.Config;

public class UserConfig {

	private Config config;
	
	public UserConfig(Config config) {
		this.config = config;
	}
	
	public String getPassword(String username) {
		return config.getString(username + ".password");
	}
	
	public int getCredits(String username) {
		return config.getInt(username + ".credits");
	}
	
	public List<String> getUsernames() {
		
		List<String> usernames = new LinkedList<String>();
		
		for (String key : config.listKeys()) {
			String clientName = key;
			if (clientName.indexOf('.') != -1) {
				clientName = clientName.substring(0, clientName.indexOf('.'));
			}
			if (!usernames.contains(clientName)) {
				usernames.add(clientName);
			}
		}
		
		return usernames;
	}
}
