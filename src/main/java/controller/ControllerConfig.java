package controller;

import util.Config;

public class ControllerConfig {

	private Config config;
	
	public ControllerConfig(Config config) {
		this.config = config;
	}
	
	public int getNodeTimeout() {
		return config.getInt("node.timeout");
	}
	
	public int getCheckPeriod() {
		return config.getInt("node.checkPeriod");
	}
	
	public int getUdpPort() {
		return config.getInt("udp.port");
	}
	
	public int getTcpPort() {
		return config.getInt("tcp.port");
	}
}
