package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;

import controller.commands.BuyCommandFactory;
import controller.commands.CommandFactory;
import controller.commands.CommandFactoryException;
import controller.commands.ComputeCommandFactory;
import controller.commands.CreditsCommandFactory;
import controller.commands.ListCommandFactory;
import controller.commands.LoginCommandFactory;
import controller.commands.LogoutCommandFactory;

public class CommandProcessor implements Runnable {

	private static HashMap<String, CommandFactory> commandFactories
			= new HashMap<String, CommandFactory>();
	static {
		commandFactories.put("!credits", new CreditsCommandFactory());
		commandFactories.put("!login", new LoginCommandFactory());
		commandFactories.put("!buy", new BuyCommandFactory());
		commandFactories.put("!logout", new LogoutCommandFactory());
		commandFactories.put("!compute", new ComputeCommandFactory());
		commandFactories.put("!list", new ListCommandFactory());
	}
	
	private Client client = null;
	
	private PrintWriter out;
	
	private BufferedReader in;
	
	private CloudController controller;
	
	public CommandProcessor(CloudController controller, Socket socket) throws IOException {
		out = new PrintWriter(socket.getOutputStream());
		in  = new BufferedReader(
				new InputStreamReader(socket.getInputStream()));
		this.controller = controller;
	}
	
	public CloudController getController() {
		return controller;
	}
	
	@Override
	public void run() {
		try {
			String command = null;
			while (!Thread.currentThread().isInterrupted() && (command = in.readLine()) != null) {
				processCommand(command);
				out.flush();
			}
		} catch (IOException e) {
			// ignored because it cannot be handled
		}
	}
	
	private void processCommand(String request) {
		
		String[] args = request.split("\\s+");
		
		if (args.length == 0) {
			return;
		}
		if (!commandFactories.containsKey(args[0])) {
			out.println("Error: Unknown command");
			return;
		}
		try {
			out.println(commandFactories.get(args[0]).create(args).execute(this));
		} catch (CommandFactoryException e) {
			out.println("Error: " + e.getMessage());
		}
	}

	public Client getClient() {
		return client;
	}
	
	public void setClient(Client client) {
		this.client = client;
	}
}
