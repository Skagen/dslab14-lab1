package node;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class ConnectionHandler implements Runnable {

	private final Logger logger;
	
	private Socket socket;
	private String operators;
	
	public ConnectionHandler(ConnectionListener connectionListener, Socket socket) {
		this.socket 	= socket;
		this.logger 	= connectionListener.getNode().getLogger();
		this.operators	= connectionListener.getNode().getConfig().getOperators();
	}
	
	@Override
	public void run() {
		PrintWriter out = null;
		try {
			out = new PrintWriter(socket.getOutputStream());
			BufferedReader in  
					= new BufferedReader(
							new InputStreamReader(socket.getInputStream()));
			
			String request = null;
			if ((request = in.readLine()) != null) {
				String[] tokens = request.split("\\s+");
				String response = null;
				if (tokens.length < 1 || !tokens[0].equals("!compute")) {
					response = renderError("unknown command");
					out.println(response);
					logger.log(request + "\n" + response);
					return;
				}
				String left = parseLeftOperand(tokens);
				if (left == null) {
					response = renderError("expected number but got " + printToken(left));
					out.println(response);
					logger.log(request + "\n" + response);
					return;
				}
				String operator = parseOperator(tokens);
				if (operator == null) {
					response = renderError("expected operator but got " + printToken(operator));
					out.println(response);
					logger.log(request + "\n" + response);
					return;
				}
				String right = parseRightOperand(tokens);
				if (right == null) {
					response = renderError("expected number but got " + printToken(right));
					out.println(response);
					logger.log(request + "\n" + response);
					return;
				}
				if (operators.indexOf(operator) == -1) {
					response = renderError("unsupported operator " + operator);
					out.println(response);
					logger.log(request + "\n" + response);
					return;
				}
				if (operator.equals("/") && right.equals("0")) {
					response = renderError("division by 0");
					out.println(response);
					logger.log(request + "\n" + response);
					return;
				}
				response = "" + evaluateTerm(left, operator, right);
				out.println(response);
				logger.log(request + "\n" + response);
			}
		} catch (IOException e) {
			System.err.println("Error while communicating with client : " + e.getMessage());
		} finally {
			if (out != null) {
				out.flush();
			}
			if (socket != null && !socket.isClosed()) {
				try {
					socket.close();
				} catch (IOException e) {
					System.err.println("Failed to close socket : " + e.getMessage());
				}
			}
		}
	}

	private String parseLeftOperand(String[] tokens) {
		if (tokens.length >= 2 && tokens[1].matches("-?[0-9]+")) {
			return tokens[1];
		}
		return null;
	}
	
	private String parseRightOperand(String[] tokens) {
		if (tokens.length == 4 && tokens[3].matches("-?[0-9]+")) {
			return tokens[3];
		}
		return null;
	}
	
	private String parseOperator(String[] tokens) {
		if (tokens.length >= 3 && tokens[2].matches("[\\+\\-\\/\\*]")) {
			return tokens[2];
		}
		return null;
	}
	
	private String renderError(String message) {
		return "Error: " + message;
	}
	
	private String printToken(String token) {
		return token == null ? "nothing" : token;
	}
	
	private int evaluateTerm(String left, String operator, String right) {
		int l = Integer.parseInt(left);
		int r = Integer.parseInt(right);
		int result = 0;
		switch (operator.charAt(0)) {
		case '+':
			result = l + r;
			break;
		case '-':
			result = l - r;
			break;
		case '*':
			result = l * r;
			break;
		case '/':
			result = l / r;
			break;
		}
		return result;
	}
}
