package node;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Logger {
	
	private String logdir;
	private String nodeName;
	
	private final ThreadLocal<SimpleDateFormat> dateFormat
			= new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat("yyyyMMdd_HHmmss.SSS");
		}
	};
	
	public Logger(Node node) { 
		logdir 		= node.getConfig().getLogDir();
		nodeName 	= node.getComponentName();
	}
	
	public void log(final String message) {
		String fileName = dateFormat.get().format(new Date()) + "_" + nodeName + ".log";
		FileWriter out = null;
		try {
			File directory = new File(logdir);
			if (!directory.exists()) {
				directory.mkdirs();
			}
			out = new FileWriter(logdir + "/" + fileName, false);
			out.write(message + "\n");
		} catch (IOException e) {
			System.err.println("failed to write to logfile : " + e.getMessage());
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) {
					System.err.println("failed to close logfile : " + e.getMessage());
				}
			}
		}
	}
}
