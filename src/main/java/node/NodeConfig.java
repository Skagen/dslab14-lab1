package node;

import util.Config;

public class NodeConfig {

	private Config config;
	
	public NodeConfig(Config config) {
		this.config = config;
	}
	
	public String getLogDir() {
		return config.getString("log.dir");
	}
	
	public  int getTcpPort() {
		return config.getInt("tcp.port");
	}
	
	public  int getAlive() {
		return config.getInt("node.alive");
	}
	
	public  String getControllerHost() {
		return config.getString("controller.host");
	}
	
	public  int getControllerUdpPort() {
		return config.getInt("controller.udp.port");
	}
	
	public  String getOperators() {
		return config.getString("node.operators");
	}
}
