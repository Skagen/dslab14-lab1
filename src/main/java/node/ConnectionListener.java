package node;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;

public class ConnectionListener implements Runnable {
	
	private Node node;
	
	private ServerSocket socket;
	private List<Socket> connections = new LinkedList<Socket>();
	
	public ConnectionListener(Node node, ServerSocket socket) {
		this.node	= node; 
		this.socket	= socket;
	}
	
	public Node getNode() {
		return node;
	}
	
	@Override
	public void run() {
		try {
			while (!Thread.currentThread().isInterrupted()) {
				Socket connection = socket.accept();
				new Thread(new ConnectionHandler(this, connection)).start();
				connections.add(connection);
				updateConnections();
			}
		} catch (IOException e) {
			// cannot be handled
		} finally {
			if (socket != null && !socket.isClosed()) {
				try {
					socket.close();
				} catch (IOException e) {
					System.err.println("Failed to close socket " + e.getMessage());
				}
			}
			closeConnections();
		}
	}

	private void updateConnections() {
		for (Socket connection : connections) {
			if (connection != null && connection.isClosed()) {
				connections.remove(connection);
			}
		}
	}
	
	private void closeConnections() {
		for (Socket connection : connections) {
			if (connection != null && !connection.isClosed()) {
				try {
					connection.close();
				} catch (IOException e) {
					System.err.println("Failed to close socket " + e.getMessage());
				}
			}
		}
		connections.clear();
	}
	
	
	public void close() {
		if (socket != null && !socket.isClosed()) {
			try {
				socket.close();
			} catch (IOException e) {
				System.err.println("Failed to close socket " + e.getMessage());
			}
		}
	}
}
