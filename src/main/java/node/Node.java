package node;

import util.Config;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import cli.Command;
import cli.Shell;

public class Node implements INodeCli, Runnable {
	
	private final String ERROR_SOCKET_CLOSE_MESSAGE
			= "failed to close socket";
	
	private NodeConfig config;
	
	private String componentName;

	private Shell shell;
	private Logger logger;
	private DatagramSocket udpSocket;
	private ServerSocket controllerSocket;
	private AliveSender aliveSender;
	private ConnectionListener connectionListener;

	/**
	 * @param componentName
	 *            the name of the component - represented in the prompt
	 * @param config
	 *            the configuration to use
	 * @param userRequestStream
	 *            the input stream to read user input from
	 * @param userResponseStream
	 *            the output stream to write the console output to
	 */
	public Node(String componentName, Config config,
			InputStream userRequestStream, PrintStream userResponseStream) {
		
		this.config = new NodeConfig(config);
		this.componentName = componentName;
		
		this.shell = new Shell(componentName, userRequestStream, userResponseStream);
		this.shell.register(this);
		
		this.logger = new Logger(this);
	}
	
	/**
	 * @return the nodes configuration object.
	 */
	public NodeConfig getConfig() {
		return config;
	}
	
	/**
	 * @return the nodes name.
	 */
	public String getComponentName() {
		return componentName;
	}
	
	/**
	 * @return the nodes logger object.
	 */
	public Logger getLogger() {
		return logger;
	}

	/**
	 * Starts the node. If the node does not start properly, it shuts down.
	 */
	@Override
	public void run() {
		try {
			controllerSocket 	= new ServerSocket(config.getTcpPort());
			udpSocket 			= new DatagramSocket();
			
			connectionListener 	= new ConnectionListener(this, controllerSocket);
			aliveSender 		= new AliveSender(this, udpSocket);
			
			new Thread(shell).start();
			new Thread(aliveSender).start();
			new Thread(connectionListener).start();
			
		} catch (IOException e) {
			shutdown();
		}
	}
	
	/**
	 * Frees all resources allocated by the node, and terminates all threads.
	 */
	public void shutdown() {
		try {
			if (controllerSocket != null) {
				controllerSocket.close();
			}
		} catch (IOException e) {
			System.err.println(ERROR_SOCKET_CLOSE_MESSAGE + " : " + e.getMessage());
		}
		if (udpSocket != null) {
			udpSocket.close();
		}
		if (connectionListener != null) {
			connectionListener.close();
		}
		if (aliveSender != null) {
			aliveSender.close();
		}
		if (shell != null) {
			shell.close();
		}
	}

	@Command
	@Override
	public String exit() throws IOException {
		shutdown();
		return null;
	}

	@Override
	public String history(int numberOfRequests) throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param args
	 *            the first argument is the name of the {@link Node} component,
	 *            which also represents the name of the configuration
	 */
	public static void main(String[] args) {
		Node node = new Node(args[0], new Config(args[0]), System.in,
				System.out);
		new Thread(node).start();
	}

	// --- Commands needed for Lab 2. Please note that you do not have to
	// implement them for the first submission. ---

	@Override
	public String resources() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

}
