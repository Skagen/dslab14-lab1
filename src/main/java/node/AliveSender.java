package node;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class AliveSender implements Runnable {
	
	private DatagramSocket socket;
	
	private int tcpPort;
	private String operators;
	private int controllerPort;
	private InetAddress controllerAddr;
	private int aliveTime;
	
	public AliveSender(Node node, DatagramSocket socket) throws UnknownHostException {
		
		this.socket = socket;
		
		tcpPort   = node.getConfig().getTcpPort();
		aliveTime = node.getConfig().getAlive();
		operators = node.getConfig().getOperators();
		controllerPort = node.getConfig().getControllerUdpPort();
		controllerAddr = InetAddress.getByName(node.getConfig().getControllerHost());
	}

	@Override
	public void run() {
		
		byte[] buffer = ("!alive " + tcpPort + " " + operators).getBytes();
		
		try {
			while (!Thread.currentThread().isInterrupted()) {
				DatagramPacket packet 
						= new DatagramPacket(
								buffer, buffer.length, controllerAddr, controllerPort);
				socket.send(packet);
				Thread.sleep(aliveTime);
			}
		} catch (IOException e) {
			// ignored because it cannot be handled
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		} finally {
			if (socket != null && !socket.isClosed()) {
				socket.close();
			}
		}
	}
	
	public void close() {
		if (socket != null) {
			socket.close();
		}
	}
}
