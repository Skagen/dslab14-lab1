*	CloudController	controller
*	Client			alice
*	Client			bill
*	Node			node1
*   Node			node2
*   Node 			node3

alice: 			!login alice 12345
>				verify("success")

bill:			!login bill 23456
>				verify("success")

controller:		!nodes
>				verify("((.*Port: 10142.*)|(.*Port: 10143.*)|(.*Port: 10144.*)){3}", T(test.util.Flag).REGEX)

bill:			!compute 1 + 2 * 3 / 9 - 5
>				verify("-4")

controller:		!nodes
>				verify("((.*10142.*100.*)|(.*10143.*50.*)|(.*10144.*50.*)){3}", T(test.util.Flag).REGEX)

alice:			!compute 1 + 2 * 3 / 9 - 5
>				verify("-4")

controller:		!nodes
>				verify("((.*10142.*200.*)|(.*10143.*100.*)|(.*10144.*100.*)){3}", T(test.util.Flag).REGEX)

controller:		!exit

node1:			!exit
node2:			!exit
node3:			!exit

bill:			!exit
alice:			!exit